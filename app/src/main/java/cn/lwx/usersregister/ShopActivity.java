package cn.lwx.usersregister;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ShopActivity extends AppCompatActivity implements View.OnClickListener {
    ItemInfo itemInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        itemInfo = new ItemInfo("宇宙无敌大宝剑", 80, 20, 30);
        findViewById(R.id.rl).setOnClickListener(this);

        TextView mLifeTV = (TextView) findViewById(R.id.tv_life);
        TextView mNameTV = (TextView) findViewById(R.id.tv_name);
        TextView mSpeedTV = (TextView) findViewById(R.id.tv_speed);
        TextView mAttackTV = (TextView) findViewById(R.id.tv_attack);
        //设置值
        mLifeTV.setText("生命值+" + itemInfo.getLife());
        mNameTV.setText(itemInfo.getName() + "");
        mSpeedTV.setText("敏捷度+" + itemInfo.getSpeed());
        mAttackTV.setText("攻击力+" + itemInfo.getAcctack());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl:
                // 1、创建意图对象
                Intent intent = new Intent();
                // 2、存储数据到意图
                intent.putExtra("equipment", itemInfo);
                // 3、把数据返回
                setResult(1, intent);
                // 4、关闭当前页面
                finish();
                break;
        }
    }
}
