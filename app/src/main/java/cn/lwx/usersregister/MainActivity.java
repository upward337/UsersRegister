package cn.lwx.usersregister;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText et_password;
    private Button btn_send;
    private EditText et_name;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1、获取相关控件
        et_name = (EditText) findViewById(R.id.et_name);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_send = (Button) findViewById(R.id.btn_send);
        //2、给按钮设置点击事件 点击开始游戏按钮进行数据传递
        btn_send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                passDate();
            }
        });
    }

    //通过这个方法实现数据的传递 传递数据
    public void passDate() {
        //1、创建Intent意图对象,启动Activity02
        //Intent intent = new Intent();
        Intent intent = new Intent(this, ShowActivity.class);
        //2、将数据存入Intent对象
        intent.putExtra("name", et_name.getText().toString().trim());
        intent.putExtra("password", et_password.getText().toString().trim());
        //3、开启意图
        startActivity(intent);
    }

    public void click(View view) {
    }
}
